package com.merapar.springxmlserver.controller;

import com.merapar.springxmlserver.rest.ParseXmlRequest;
import com.merapar.springxmlserver.rest.ParseXmlResponse;
import com.merapar.springxmlserver.rest.ResponseDetails;
import com.merapar.springxmlserver.service.ParseXmlDetails;
import com.merapar.springxmlserver.service.ParseXmlResult;
import com.merapar.springxmlserver.service.ParseXmlService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ParseXmlController {

    @RequestMapping(value = "/analyze", produces = "application/json")
    private ParseXmlResponse parseXml(@RequestBody ParseXmlRequest request){
        ParseXmlService parseXmlService = new ParseXmlService();
        ParseXmlResult parseXmlResult = parseXmlService.parseXml(request.getUrl());
        ParseXmlResponse parseXmlResponse = convertResultToResponse(parseXmlResult);
        return parseXmlResponse;
    }

    public ParseXmlResponse convertResultToResponse(ParseXmlResult result){

        ParseXmlResponse parseXmlResponse = new ParseXmlResponse();
        parseXmlResponse.setAnalyseDate(result.getAnalyseDate());
        parseXmlResponse.setDetails(convertResultDetailsToResultResponse(result.getDetails()));
        return parseXmlResponse;
    }

    ResponseDetails convertResultDetailsToResultResponse(ParseXmlDetails details){

        ResponseDetails responseDetails = new ResponseDetails();
        responseDetails.setFirstPost(details.getFirstPost());
        responseDetails.setLastPost(details.getLastPost());
        responseDetails.setAvgScore(details.getAvgScore());
        responseDetails.setTotalAcceptedPosts(details.getTotalAcceptedPosts());
        responseDetails.setTotalPosts(details.getTotalPosts());

        return responseDetails;
    }
}
