package com.merapar.springxmlserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringxmlserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringxmlserverApplication.class, args);
    }

}
