package com.merapar.springxmlserver.rest;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.sun.istack.internal.NotNull;

@JsonDeserialize(builder = ParseXmlRequest.Builder.class)
public class ParseXmlRequest {
    @NotNull
    private String url;

    private ParseXmlRequest() {
    }

    public String getUrl() {
        return url;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String url;

        public Builder() {
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public ParseXmlRequest build() {
            ParseXmlRequest request = new ParseXmlRequest();

            request.url = this.url;
            return request;
        }
    }
}
