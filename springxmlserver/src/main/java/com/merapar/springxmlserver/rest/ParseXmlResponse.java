package com.merapar.springxmlserver.rest;

import java.time.LocalDateTime;

public class ParseXmlResponse {

    private LocalDateTime analyseDate;
    private ResponseDetails details;

    public LocalDateTime getAnalyseDate() {
        return analyseDate;
    }

    public void setAnalyseDate(LocalDateTime analyseDate) {
        this.analyseDate = analyseDate;
    }

    public ResponseDetails getDetails() {
        return details;
    }

    public void setDetails(ResponseDetails details) {
        this.details = details;
    }
}
