package com.merapar.springxmlserver.demo.exceptions;

/* Exception class to catch runtime exceptions
 */
public class ServiceException extends RuntimeException {
    public ServiceException(String message) {
        super(message);
    }
}
