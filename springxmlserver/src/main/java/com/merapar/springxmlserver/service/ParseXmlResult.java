package com.merapar.springxmlserver.service;

import java.time.LocalDateTime;

public class ParseXmlResult {

    public ParseXmlResult() {
    }

    private LocalDateTime analyseDate;
    private ParseXmlDetails details;

    public LocalDateTime getAnalyseDate() {
        return analyseDate;
    }

    public ParseXmlDetails getDetails() {
        return details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ParseXmlResult)) return false;

        ParseXmlResult that = (ParseXmlResult) o;

        return (analyseDate != null ? analyseDate.equals(that.analyseDate) : that.analyseDate == null) && (details != null ? details.equals(that.details) : that.details == null);
    }

    @Override
    public int hashCode() {
        int result = analyseDate != null ? analyseDate.hashCode() : 0;
        result = 31 * result + (details != null ? details.hashCode() : 0);
        return result;
    }

    public static final class Builder {
        private LocalDateTime analyseDate;
        private ParseXmlDetails details;

        public Builder() {
            this.analyseDate = LocalDateTime.now();
        }

        public Builder analyseDate(LocalDateTime analyseDate) {
            this.analyseDate = analyseDate;
            return this;
        }

        public Builder details(ParseXmlDetails details) {
            this.details = details;
            return this;
        }

        public ParseXmlResult build() {
            ParseXmlResult parseXmlResult = new ParseXmlResult();
            parseXmlResult.analyseDate = this.analyseDate;
            parseXmlResult.details = this.details;
            return parseXmlResult;
        }
    }
}

