package com.merapar.springxmlserver.service;

import com.merapar.springxmlserver.demo.exceptions.ServiceException;
import org.springframework.stereotype.Component;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;

@Component
public class ParseXmlService {

    public ParseXmlService() {
    }

    public ParseXmlResult parseXml(String requestUrl) {

        int totalPosts = 0;
        int acceptedPosts = 0;
        boolean firstRow = true;
        double scoreSum = 0;
        LocalDateTime lastPost = null;
        ParseXmlResult.Builder builder = new ParseXmlResult.Builder();
        ParseXmlDetails.Builder detailsBuilder = new ParseXmlDetails.Builder();

        try {
            URL url = new URL(requestUrl);
            URLConnection con = url.openConnection();
            InputStream inputStream = new BufferedInputStream(con.getInputStream(), 524288);
            XMLStreamReader streamReader = initializeStreamReader(inputStream);

            while (streamReader.hasNext()) {
                int eventType = streamReader.next();
                switch (eventType) {
                    case XMLStreamReader.START_ELEMENT:
                        String elementName = streamReader.getLocalName();
                        if (elementName.equals("row")) {
                            String creationDate = streamReader.getAttributeValue(null, "CreationDate");
                            String id = streamReader.getAttributeValue(null, "Id");
                            String score = streamReader.getAttributeValue(null, "Score");
                            if (id == null || creationDate == null || score == null) {
                                break;
                            }
                            lastPost = LocalDateTime.parse(creationDate);
                            if (firstRow) {
                                detailsBuilder = detailsBuilder.firstPost(lastPost);
                                firstRow = false;
                            }
                            if (streamReader.getAttributeValue(null, "AcceptedAnswerId") != null) {
                                acceptedPosts++;
                            }
                            totalPosts++;
                            scoreSum += Double.parseDouble(score);

                        }
                        break;
                    case XMLStreamReader.END_ELEMENT:
                        break;
                }
            }
        } catch (XMLStreamException ex) {
            throw new ServiceException(ex.getMessage());
        } catch (MalformedURLException ex) {
            throw new ServiceException(ex.getMessage());
        } catch (IOException ex) {
            throw new ServiceException(ex.getMessage());
        }

        builder.details(detailsBuilder
                .lastPost(lastPost)
                .totalAcceptedPosts(acceptedPosts)
                .totalPosts(totalPosts)
                .avgScore(scoreSum / totalPosts)
                .build())
                .analyseDate(LocalDateTime.now());
        return builder.build();
    }

    private XMLStreamReader initializeStreamReader(InputStream inputStream) {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        try {
            return inputFactory.createXMLStreamReader(inputStream);
        } catch (XMLStreamException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    private void closeStreamReader(XMLStreamReader reader, InputStream resource) {
        try {
            reader.close();
        } catch (XMLStreamException ex) {
            throw new ServiceException(ex.getMessage());
        } finally {
            try {
                resource.close();
            } catch (IOException ex) {
                throw new ServiceException(ex.getMessage());
            }
        }
    }
}
