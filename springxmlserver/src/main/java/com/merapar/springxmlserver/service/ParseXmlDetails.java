package com.merapar.springxmlserver.service;

import java.time.LocalDateTime;

public class ParseXmlDetails {

    private LocalDateTime firstPost;
    private LocalDateTime lastPost;
    private int totalPosts;
    private int totalAcceptedPosts;
    private double avgScore;

    public ParseXmlDetails(){

    }

    public LocalDateTime getFirstPost() {
        return firstPost;
    }

    public LocalDateTime getLastPost() {
        return lastPost;
    }

    public int getTotalPosts() {
        return totalPosts;
    }

    public int getTotalAcceptedPosts() {
        return totalAcceptedPosts;
    }

    public double getAvgScore() {
        return avgScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ParseXmlDetails)) return false;

        ParseXmlDetails that = (ParseXmlDetails) o;

        if (totalPosts != that.totalPosts) return false;
        if (totalAcceptedPosts != that.totalAcceptedPosts) return false;
        if (Double.compare(that.avgScore, avgScore) != 0) return false;
        return (firstPost != null ? firstPost.equals(that.firstPost) : that.firstPost == null) && (lastPost != null ? lastPost.equals(that.lastPost) : that.lastPost == null);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = firstPost != null ? firstPost.hashCode() : 0;
        result = 31 * result + (lastPost != null ? lastPost.hashCode() : 0);
        result = 31 * result + totalPosts;
        result = 31 * result + totalAcceptedPosts;
        temp = Double.doubleToLongBits(avgScore);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public static final class Builder {
        private LocalDateTime firstPost;
        private LocalDateTime lastPost;
        private int totalPosts;
        private int totalAcceptedPosts;
        private double avgScore;

        public Builder() {
        }

        public Builder firstPost(LocalDateTime firstPost) {
            this.firstPost = firstPost;
            return this;
        }

        public Builder lastPost(LocalDateTime lastPost) {
            this.lastPost = lastPost;
            return this;
        }

        public Builder totalPosts(int totalPosts) {
            this.totalPosts = totalPosts;
            return this;
        }

        public Builder totalAcceptedPosts(int totalAcceptedPosts) {
            this.totalAcceptedPosts = totalAcceptedPosts;
            return this;
        }

        public Builder avgScore(double avgScore) {
            this.avgScore = avgScore;
            return this;
        }

        public ParseXmlDetails build() {
            ParseXmlDetails parseXmlDetails = new ParseXmlDetails();
            parseXmlDetails.lastPost = this.lastPost;
            parseXmlDetails.firstPost = this.firstPost;
            parseXmlDetails.avgScore = this.avgScore;
            parseXmlDetails.totalAcceptedPosts = this.totalAcceptedPosts;
            parseXmlDetails.totalPosts = this.totalPosts;
            return parseXmlDetails;
        }
    }
}

