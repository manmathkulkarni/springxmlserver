package com.merapar.springxmlserver.service;

import com.merapar.springxmlserver.demo.exceptions.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URL;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ParseXmlServiceTests {

    private static final String REQUEST_URL1 = "https://merapar-assessment-task.s3.eu-central-1.amazonaws.com/arabic-posts.xml";
    private static final String REQUEST_URL2 = "https://merapar-assessment-task.s3.eu-central-1.amazonaws.com/3dprinting-posts.xml";

    private static final String malformedUrl = "hps://merapar-assessment-task.s3.eu-central-1.amazonaws.com/arabic-posts.xml";

    private static final String URL1 = "/Posts.xml";

    @InjectMocks
    ParseXmlService parseXmlService;

    @Test
    public void testParseArabicPosts_Success() {

        //Create xml details
        ParseXmlDetails parseXmlDetails = new ParseXmlDetails.Builder()
                .firstPost(LocalDateTime.parse("2015-07-14T18:39:27.757"))
                .lastPost(LocalDateTime.parse("2015-09-14T12:46:52.053"))
                .avgScore(2.975)
                .totalAcceptedPosts(7)
                .totalPosts(80)
                .build();
        ParseXmlResult expected = new ParseXmlResult.Builder().analyseDate(LocalDateTime.now()).details(parseXmlDetails).build();

        ParseXmlResult actual = parseXmlService.parseXml(REQUEST_URL1);

        assertEquals(expected.getDetails().getFirstPost(), actual.getDetails().getFirstPost());
        assertEquals(expected.getDetails().getLastPost(), actual.getDetails().getLastPost());
        assertEquals(expected.getDetails().getTotalPosts(), actual.getDetails().getTotalPosts());
        assertEquals(expected.getDetails().getTotalAcceptedPosts(), actual.getDetails().getTotalAcceptedPosts());
        assertEquals(expected.getDetails().getAvgScore(), actual.getDetails().getAvgScore(),0);
    }

    @Test
    public void testParse3DPrintingPosts_Success() {
        //Create xml details
        ParseXmlDetails parseXmlDetails = new ParseXmlDetails.Builder()
                .firstPost(LocalDateTime.parse("2016-01-12T19:24:29.457"))
                .lastPost(LocalDateTime.parse("2021-02-21T09:29:40.710"))
                .avgScore(2.641851106639839)
                .totalAcceptedPosts(65)
                .totalPosts(497)
                .build();
        ParseXmlResult expected = new ParseXmlResult.Builder().analyseDate(LocalDateTime.now()).details(parseXmlDetails).build();

        ParseXmlResult actual = parseXmlService.parseXml(REQUEST_URL2);

        assertEquals(expected.getDetails().getFirstPost(), actual.getDetails().getFirstPost());
        assertEquals(expected.getDetails().getLastPost(), actual.getDetails().getLastPost());
        assertEquals(expected.getDetails().getTotalPosts(), actual.getDetails().getTotalPosts());
        assertEquals(expected.getDetails().getTotalAcceptedPosts(), actual.getDetails().getTotalAcceptedPosts());
        assertEquals(expected.getDetails().getAvgScore(), actual.getDetails().getAvgScore(),0);
    }

    @Test
    public void testBigXmFile() {
        //Create xml details
        ParseXmlDetails parseXmlDetails = new ParseXmlDetails.Builder()
                .firstPost(LocalDateTime.parse("2016-01-12T18:45:19.963"))
                .lastPost(LocalDateTime.parse("2021-02-28T02:32:54.073"))
                .avgScore(2.7590053026147374)
                .totalAcceptedPosts(1878)
                .totalPosts(10938)
                .build();
        ParseXmlResult expected = new ParseXmlResult.Builder().analyseDate(LocalDateTime.now()).details(parseXmlDetails).build();

        URL url = Test.class.getResource("/Posts.xml");
        ParseXmlResult actual = parseXmlService.parseXml(url.toString());


        assertEquals(expected.getDetails().getFirstPost(), actual.getDetails().getFirstPost());
        assertEquals(expected.getDetails().getLastPost(), actual.getDetails().getLastPost());
        assertEquals(expected.getDetails().getTotalPosts(), actual.getDetails().getTotalPosts());
        assertEquals(expected.getDetails().getTotalAcceptedPosts(), actual.getDetails().getTotalAcceptedPosts());
        assertEquals(expected.getDetails().getAvgScore(), actual.getDetails().getAvgScore(),0);
    }

    @Test(expected = ServiceException.class)
    public void testUrlWithWrongProtocol(){
        ParseXmlResult xmlResult = parseXmlService.parseXml(malformedUrl);
    }

}